@pet
Feature: Pet Store features

  // @influxdb

  Scenario Outline: Create a new pet
    When I request to create a pet
    Then I should get <expectedStatusCode> status code
    And The value for the "<key>" after post operation should be "<value>"

    Examples:
      | key  | value   | expectedStatusCode |
      | name | doggiee | 200                |

  @updatepet
  Scenario Outline: Update an existing  pet
    When I request to update a pet by id "<id>"
    Then I should get <expectedStatusCode> status code
    And The value for the "<key>" after update operation should be "<value>"

    Examples:
      | id | key  | value | expectedStatusCode |
      | 3  | name | Cat3 | 200                |

  Scenario Outline: Find pet by status
    When I request to get a pet by status "<status>"
    Then I should get <expectedStatusCode> status code
    And The value for the "<key>" after get operation should be "<value>"
    Examples:
      | status | expectedStatusCode | key  | value               |
      | sold   | 200                | name | [dolore exercitation, SPARROW, newly updated doggies, factory pup, Charlie] |


  Scenario Outline: Find pet by id
    When I request to get a pet by id "<id>"
    Then I should get <expectedStatusCode> status code
    And The value for the "<key>" after get operation should be "<value>"
    Examples:
      | id | expectedStatusCode | key  | value |
      | 2  | 200                | name | Rat |

  Scenario: Delete an existing pet
    When I request to delete a pet by id "3002"
    Then I should get 200 status code

  Scenario Outline: Updates a pet in the store with form data
    When I request to update a pet by "<id>"
#    """
#      {
#        "name": "dog6",
#        "status": "sold"
#      }
#
#    """
    Then I should get <expectedStatusCode> status code
    And The value for the "<key>" after get operation should be "<value>"
    Examples:
      | id | expectedStatusCode | key    | value   |
      | 6  | 200                | status | pending |






