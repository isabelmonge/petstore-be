@user
Feature: Users Features


  Scenario Outline: Create user
    When I request to create a user
    Then I must get <expectedStatCode> stat code
    And The value for the "<attribute>" after post operation must be "<value>"
    Examples:
      | expectedStatCode | attribute | value |
      | 200              | username  | Mary  |

  Scenario: Delete an existing user
    When I request to delete a user by username "Mary"
    Then I must get 200 stat code