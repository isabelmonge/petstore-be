package com.pet.gherkinDefinitions;


import com.pet.influxdb.InfluxDBIntegration;
import com.pet.serenitySteps.UserSteps;
import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

import java.util.Calendar;

public class UsersDefinitions {

    /**
     * Method executed before each scenario to start measuring execution times
     *
     * @param scenario Scenario object to check if the scenario contains the tag to write on InfluxDB
     */
    @Before
    public void startInfluxdb(Scenario scenario) {
        if (scenario.getSourceTagNames().contains("@influxdb"))
            Serenity.setSessionVariable("startTime").to(Calendar.getInstance());
    }

    @Steps
    private UserSteps userSteps;

    @When("^I request to create a user$")
    public void iRequestToCreateAUser() {
        // Write code here that turns the phrase above into concrete actions
        userSteps.createUser();
    }


    @Then("^I must get (.*) stat code$")
    public void iMustGetExpectedStatCodeStatusCode(int expectedStatCode) {
        // Write code here that turns the phrase above into concrete actions
        userSteps.verifyStatCode(expectedStatCode);
    }

    @And("^The value for the \"([^\"]*)\" after (get|post|delete) operation must be \"([^\"]*)\"$")
    public void theValueForTheAfterPostOperationMustBe(String attribute, String operation, String expectedValue) {
        Response res = Serenity.sessionVariableCalled("response");
        userSteps.verifyValue(res, operation, attribute, expectedValue);
    }



    @After
    public void measureScenario(Scenario scenario) {

        if (scenario.getSourceTagNames().contains("@influxdb")) {
            InfluxDBIntegration bd = InfluxDBIntegration.getInstance();
            Calendar startTime = Serenity.sessionVariableCalled("startTime");
            Serenity.setSessionVariable("endTime").to(Calendar.getInstance());
            Calendar endTime = Serenity.sessionVariableCalled("endTime");
            bd.writeInfluxDB(scenario, startTime, endTime);
        }

    }

    @When("^I request to delete a user by username \"([^\"]*)\"$")
    public void iRequestToDeleteAUserByUsername(String username) {
        // Write code here that turns the phrase above into concrete actions
        userSteps.deleteUserByUsername(username);
    }


}
