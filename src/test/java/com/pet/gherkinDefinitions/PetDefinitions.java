package com.pet.gherkinDefinitions;

import com.pet.influxdb.InfluxDBIntegration;
import com.pet.serenitySteps.PetSteps;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;


import java.util.Calendar;

public class PetDefinitions {

    /**
     * Method executed before each scenario to start measuring execution times
     *
     * @param scenario Scenario object to check if the scenario contains the tag to write on InfluxDB
     */
    @Before
    public void startInfluxdb(Scenario scenario) {
        if (scenario.getSourceTagNames().contains("@influxdb"))
            Serenity.setSessionVariable("startTime").to(Calendar.getInstance());
    }

    @Steps
    private PetSteps petSteps;




    @When("^I request to create a pet$")
    public void iRequestToCreateAPet() {
        // Write code here that turns the phrase above into concrete actions
        petSteps.createPet();

    }


    @When("^I request to (get|update|delete) a pet by status \"([^\"]*)\"$")
    public void iRequestToOperAPetByStatus(String operation, String status) {
        // Write code here that turns the phrase above into concrete actions
        switch (operation.toLowerCase()) {
            case "get":
                petSteps.getPetByStatus(status);
                break;
            case "update":
                petSteps.updatePetByStatus(status);
                break;
            case "delete":
                //petSteps.deletePetByStatus(status);
                break;
            default:
                break;
        }
    }

    @Then("I should get (.*) status code")
    public void iShouldGetStatusCode(int expectedStatusCode) {
        petSteps.verifyStatusCode(expectedStatusCode);
    }

    @And("^The value for the \"([^\"]*)\" after (get|post|update|delete) operation should be \"([^\"]*)\"$")
    public void theValueForTheAfterGetOperationShouldBe(String key, String operation, String expectedValue) {
        Response res = Serenity.sessionVariableCalled("response");
        petSteps.verifyValueFromKey(res, operation, key, expectedValue);
    }

    @After
    public void measureScenario(Scenario scenario) {

        if (scenario.getSourceTagNames().contains("@influxdb")) {
            InfluxDBIntegration bd = InfluxDBIntegration.getInstance();
            Calendar startTime = Serenity.sessionVariableCalled("startTime");
            Serenity.setSessionVariable("endTime").to(Calendar.getInstance());
            Calendar endTime = Serenity.sessionVariableCalled("endTime");
            bd.writeInfluxDB(scenario, startTime, endTime);
        }

    }

    @When("^I request to (get|update|delete) a pet by id \"([^\"]*)\"$")
    public void iRequestToOperAPetById(String operation, String id) {
        // Write code here that turns the phrase above into concrete actions
        switch (operation.toLowerCase()) {
            case "get":
                petSteps.getPetById(id);
                break;
            case "update":
                petSteps.updatePetById(id);
                break;
            case "delete":
                petSteps.deletePetById(id);
                break;
            default:
                break;
        }
    }


    @When("^I request to delete a pet by id$")
    public void iRequestToDeleteAPetById(String id)  {
        // Write code here that turns the phrase above into concrete actions
        petSteps.deletePetById(id);
    }

//    @Given("^I search uri \"([^\"]*)\"$")
//    public void iSearchUri(String uri) {
//        // Write code here that turns the phrase above into concrete actions
//        petSteps.searchUri(uri);
//    }


    @When("^I request to update a pet by \"([^\"]*)\"$")
    public void iRequestToUpdateAPetBy(String id)  {
        // Write code here that turns the phrase above into concrete actions
        petSteps.updatePet(id);
    }

}

