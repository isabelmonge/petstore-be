package com.pet.serenitySteps;

import com.pet.config.ServicesConfiguration;
import com.pet.support.ServicesSupport;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import org.json.JSONObject;
import org.junit.Assert;

import java.io.InputStream;

import static net.serenitybdd.rest.SerenityRest.rest;

public class UserSteps {

    private ServicesSupport servicesSupport = new ServicesSupport();
    private RequestSpecification specUsers = rest().baseUri(ServicesConfiguration.URI).contentType(ContentType.JSON).when();

    public String getEndpointUsers() {

        return endpoint;
    }

    private String endpoint = ServicesConfiguration.USERS;


    /**
     * Performs a POST operation that will create a new user
     */
    @Step
    public void createUser() {

        try {
            InputStream is = this.getClass().getResourceAsStream("/requests/add_user.json");
            JSONObject body = servicesSupport.jsonInputStreamToJsonObject(is);
            specUsers = specUsers.body(body.toMap());
            Response response = servicesSupport.executeRequest(specUsers, "POST", endpoint);
            Serenity.setSessionVariable("response").to(response);
        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Step
    public void verifyStatCode(int expectedStatCode) {

            Response res = Serenity.sessionVariableCalled("response");
            Assert.assertEquals("status code doesn't match", expectedStatCode, res.getStatusCode());
    }

    @Step
    public void verifyValue(Response res, String operation, String attribute, String expectedValue) {

        String currentValue = "";

        switch (operation.toLowerCase()) {
            case "get":
                currentValue = res.getBody().jsonPath().getString(attribute);
                break;
            case "post":
                currentValue = res.getBody().jsonPath().getString(attribute);
                break;
//            case "update":
//                currentValue = res.getBody().jsonPath().getString(key);
//                break;
            case "delete":
                currentValue = res.getBody().jsonPath().getString(attribute);
                break;
            default:
                break;
        }

        Assert.assertEquals("Value for " + attribute + " doesn't match", expectedValue, currentValue);
    }

    @Step
    public void deleteUserByUsername(String username) {

        String endpoint = getEndpointUsers() + "/" + username;
        Response response = servicesSupport.executeRequest(specUsers, "DELETE", endpoint);
        Serenity.setSessionVariable("response").to(response);
    }




}
