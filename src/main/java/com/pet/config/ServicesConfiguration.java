package com.pet.config;

public class ServicesConfiguration{

    // Base URI
    public static final String URI = "http://petstore.swagger.io/v2";


    // Endpoints
    public static final String PET = "/pet";
    public static final String USERS = "/user";

}